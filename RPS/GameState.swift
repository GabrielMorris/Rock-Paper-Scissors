import Foundation

enum GameState {
    case start
    case win
    case lose
    case draw
    
    var statusMessage: String {
        switch self {
        case .start:
            return "Rock, Paper, Scissors?"
        case .win:
            return "You won!"
        case .lose:
            return "You lose!"
        case .draw:
            return "It's a draw!"
        }
    }
}
