import Foundation
import GameplayKit

let randomChoice = GKRandomDistribution(lowestValue: 0, highestValue: 2)

func randomSign() -> Sign {
    let sign = randomChoice.nextInt()
    if sign == 0 {
        return .rock
    } else if  sign == 1 {
        return .paper
    } else {
        return .scissors
    }
}

enum Sign {
    case rock
    case paper
    case scissors
    
    var usedSign: String {
        switch self {
        case .rock:
            return "👊🏿"
        case .paper:
            return "✋🏿"
        case .scissors:
            return "✌🏿"
        }
    }
    
    func compareSigns(versusSign: Sign) -> GameState {
        if self == versusSign {
            return GameState.draw
        }
        switch self {
        case .rock:
            if versusSign == .paper {
                return .win
            }
        case .paper:
            if versusSign == .rock {
                return .win
            }
        case .scissors:
            if versusSign == .scissors {
                return .win
            }
        }
        return GameState.lose
        
    }
}
