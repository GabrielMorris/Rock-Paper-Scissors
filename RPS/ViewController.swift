import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        modifyUI(gamestate: .start)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func modifyUI(gamestate: GameState) {
        switch gamestate {
        case .start:
            appSign.text = "🤡"
            gameStatus.text = gamestate.statusMessage
            view.backgroundColor = .white
            playAgainLabel.isHidden = true
            rockLabel.isHidden = false
            paperLabel.isHidden = false
            scissorLabel.isHidden = false
            rockLabel.isEnabled = true
            paperLabel.isEnabled = true
            scissorLabel.isEnabled = true
        case .win:
            gameStatus.text = gamestate.statusMessage
            view.backgroundColor = .green
        case .lose:
            gameStatus.text = gamestate.statusMessage
            view.backgroundColor = .red
        case .draw:
            gameStatus.text = gamestate.statusMessage
            view.backgroundColor = .yellow
        }
    }
    
    func play(_ playerSign: Sign) {
        let AISign = randomSign()
        let gameState = playerSign.compareSigns(versusSign: AISign)
        modifyUI(gamestate: gameState)
        appSign.text = AISign.usedSign
        
        rockLabel.isHidden = true
        rockLabel.isEnabled = false
        paperLabel.isHidden = true
        paperLabel.isEnabled = false
        scissorLabel.isHidden = true
        scissorLabel.isEnabled = false
        playAgainLabel.isHidden = false
        playAgainLabel.isEnabled = true
        
        switch playerSign {
        case .rock:
            rockLabel.isHidden = false
        case .paper:
            paperLabel.isHidden = false
        case .scissors:
            scissorLabel.isHidden = false
        }
    }
    
    @IBOutlet weak var appSign: UILabel!
    @IBOutlet weak var gameStatus: UILabel!
    @IBOutlet weak var rockLabel: UIButton!
    @IBOutlet weak var paperLabel: UIButton!
    @IBOutlet weak var scissorLabel: UIButton!
    @IBOutlet weak var playAgainLabel: UIButton!

    @IBAction func rockButton(_ sender: Any) {
        play(.rock)
    }
    @IBAction func paperButton(_ sender: Any) {
    play(.paper)
    }
    @IBAction func scissorButton(_ sender: Any) {
    play(.scissors)
    }
    @IBAction func playAgainButton(_ sender: Any) {
        modifyUI(gamestate: .start)
    }

}

